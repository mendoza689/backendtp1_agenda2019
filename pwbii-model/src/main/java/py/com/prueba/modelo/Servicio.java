package py.com.prueba.modelo;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;


/**
 * The persistent class for the servicio database table.
 * 
 */
@Entity
@NamedQuery(name="Servicio.findAll", query="SELECT s FROM Servicio s")
@JsonIgnoreProperties({"especialidad","duracionReferencia"})
public class Servicio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SERVICIO_IDSERVICIO_GENERATOR", sequenceName="SERVICIO_SEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SERVICIO_IDSERVICIO_GENERATOR")
	@Column(name="id_servicio")
	private Integer idServicio;

	@Column(name="duracion_referencia")
	private Integer duracionReferencia;

	private String nombre;

	//bi-directional many-to-one association to Especialidad
	@ManyToOne
	@JoinColumn(name="id_especialidad")
	private Especialidad especialidad;

	//bi-directional many-to-one association to SucursalServicio
	@OneToMany(mappedBy="servicio",fetch = FetchType.EAGER)
	private List<SucursalServicio> sucursalServicios;

	public Servicio() {
	}

	public Integer getIdServicio() {
		return this.idServicio;
	}

	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

	public Integer getDuracionReferencia() {
		return this.duracionReferencia;
	}

	public void setDuracionReferencia(Integer duracionReferencia) {
		this.duracionReferencia = duracionReferencia;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Especialidad getEspecialidad() {
		return this.especialidad;
	}

	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}

	public List<SucursalServicio> getSucursalServicios() {
		return this.sucursalServicios;
	}

	public void setSucursalServicios(List<SucursalServicio> sucursalServicios) {
		this.sucursalServicios = sucursalServicios;
	}

	public SucursalServicio addSucursalServicio(SucursalServicio sucursalServicio) {
		getSucursalServicios().add(sucursalServicio);
		sucursalServicio.setServicio(this);

		return sucursalServicio;
	}

	public SucursalServicio removeSucursalServicio(SucursalServicio sucursalServicio) {
		getSucursalServicios().remove(sucursalServicio);
		sucursalServicio.setServicio(null);

		return sucursalServicio;
	}

}