package py.com.prueba.modelo;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;


/**
 * The persistent class for the especialidad database table.
 * 
 */
@Entity
@NamedQuery(name="Especialidad.findAll", query="SELECT e FROM Especialidad e")
@JsonIgnoreProperties({"sucursalServicios"})
public class Especialidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ESPECIALIDAD_IDESPECIALIDAD_GENERATOR", sequenceName="ESPECIALIDAD_SEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ESPECIALIDAD_IDESPECIALIDAD_GENERATOR")
	@Column(name="id_especialidad")
	private Integer idEspecialidad;

	private String nombre;

	//bi-directional many-to-one association to Servicio
	@OneToMany(mappedBy="especialidad",fetch = FetchType.EAGER)
	private List<Servicio> servicios;

	public Especialidad() {
	}

	public Integer getIdEspecialidad() {
		return this.idEspecialidad;
	}

	public void setIdEspecialidad(Integer idEspecialidad) {
		this.idEspecialidad = idEspecialidad;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Servicio> getServicios() {
		return this.servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;
	}

	public Servicio addServicio(Servicio servicio) {
		getServicios().add(servicio);
		servicio.setEspecialidad(this);

		return servicio;
	}

	public Servicio removeServicio(Servicio servicio) {
		getServicios().remove(servicio);
		servicio.setEspecialidad(null);

		return servicio;
	}

}