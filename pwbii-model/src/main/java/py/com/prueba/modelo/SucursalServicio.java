package py.com.prueba.modelo;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the sucursal_servicio database table.
 * 
 */
@Entity
@Table(name="sucursal_servicio")
@NamedQuery(name="SucursalServicio.findAll", query="SELECT s FROM SucursalServicio s")
@JsonIgnoreProperties({"personaSucursalServicios","duracion","precio","reservas","sucursal","servicio","capacidad"})
public class SucursalServicio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SUCURSAL_SERVICIO_IDSUCURSALSERVICIO_GENERATOR", sequenceName="SUCURSAL_SERVICIO_SEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SUCURSAL_SERVICIO_IDSUCURSALSERVICIO_GENERATOR")
	@Column(name="id_sucursal_servicio")
	private Integer idSucursalServicio;

	private Integer capacidad;

	private Integer duracion;

	private BigDecimal precio;

	//bi-directional many-to-one association to PersonaSucursalServicio
	@OneToMany(mappedBy="sucursalServicio")
	private List<PersonaSucursalServicio> personaSucursalServicios;

	//bi-directional many-to-one association to Reserva
	@OneToMany(mappedBy="sucursalServicio")
	private List<Reserva> reservas;

	//bi-directional many-to-one association to Servicio
	@ManyToOne
	@JoinColumn(name="id_servicio")
	private Servicio servicio;

	//bi-directional many-to-one association to Sucursal
	@ManyToOne
	@JoinColumn(name="id_sucursal")
	private Sucursal sucursal;

	public SucursalServicio() {
	}

	public Integer getIdSucursalServicio() {
		return this.idSucursalServicio;
	}

	public void setIdSucursalServicio(Integer idSucursalServicio) {
		this.idSucursalServicio = idSucursalServicio;
	}

	public Integer getCapacidad() {
		return this.capacidad;
	}

	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}

	public Integer getDuracion() {
		return this.duracion;
	}

	public void setDuracion(Integer duracion) {
		this.duracion = duracion;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public List<PersonaSucursalServicio> getPersonaSucursalServicios() {
		return this.personaSucursalServicios;
	}

	public void setPersonaSucursalServicios(List<PersonaSucursalServicio> personaSucursalServicios) {
		this.personaSucursalServicios = personaSucursalServicios;
	}

	public PersonaSucursalServicio addPersonaSucursalServicio(PersonaSucursalServicio personaSucursalServicio) {
		getPersonaSucursalServicios().add(personaSucursalServicio);
		personaSucursalServicio.setSucursalServicio(this);

		return personaSucursalServicio;
	}

	public PersonaSucursalServicio removePersonaSucursalServicio(PersonaSucursalServicio personaSucursalServicio) {
		getPersonaSucursalServicios().remove(personaSucursalServicio);
		personaSucursalServicio.setSucursalServicio(null);

		return personaSucursalServicio;
	}

	public List<Reserva> getReservas() {
		return this.reservas;
	}

	public void setReservas(List<Reserva> reservas) {
		this.reservas = reservas;
	}

	public Reserva addReserva(Reserva reserva) {
		getReservas().add(reserva);
		reserva.setSucursalServicio(this);

		return reserva;
	}

	public Reserva removeReserva(Reserva reserva) {
		getReservas().remove(reserva);
		reserva.setSucursalServicio(null);

		return reserva;
	}

	public Servicio getServicio() {
		return this.servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

	public Sucursal getSucursal() {
		return this.sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

}