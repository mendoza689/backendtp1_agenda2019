package py.com.prueba.modelo;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;


/**
 * The persistent class for the horario_excepcion database table.
 * 
 */
@Entity
@Table(name="horario_excepcion")
@NamedQuery(name="HorarioExcepcion.findAll", query="SELECT h FROM HorarioExcepcion h")
public class HorarioExcepcion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="HORARIO_EXCEPCION_IDHORARIOEXCEPCION_GENERATOR", sequenceName="HORARIO_EXCEPCION_SEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HORARIO_EXCEPCION_IDHORARIOEXCEPCION_GENERATOR")
	@Column(name="id_horario_excepcion")
	private Integer idHorarioExcepcion;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Column(name="hora_apertura")
	private Time horaApertura;

	@Column(name="hora_cierre")
	private Time horaCierre;

	//bi-directional many-to-one association to Sucursal
	@ManyToOne
	@JoinColumn(name="id_sucursal")
	private Sucursal sucursal;

	public HorarioExcepcion() {
	}

	public Integer getIdHorarioExcepcion() {
		return this.idHorarioExcepcion;
	}

	public void setIdHorarioExcepcion(Integer idHorarioExcepcion) {
		this.idHorarioExcepcion = idHorarioExcepcion;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Time getHoraApertura() {
		return this.horaApertura;
	}

	public void setHoraApertura(Time horaApertura) {
		this.horaApertura = horaApertura;
	}

	public Time getHoraCierre() {
		return this.horaCierre;
	}

	public void setHoraCierre(Time horaCierre) {
		this.horaCierre = horaCierre;
	}

	public Sucursal getSucursal() {
		return this.sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

}