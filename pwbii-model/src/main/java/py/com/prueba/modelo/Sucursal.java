package py.com.prueba.modelo;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Time;
import java.util.List;


/**
 * The persistent class for the sucursal database table.
 * 
 */
@Entity
@NamedQuery(name="Sucursal.findAll", query="SELECT s FROM Sucursal s")
@JsonIgnoreProperties({"horarioExcepcions","domingoHoraApertura","domingoHoraCierre","juevesHoraApertura","juevesHoraCierre","lunesHoraApertura",
	"lunesHoraCierre","martesHoraApertura","martesHoraCierre","miercolesHoraApertura","miercolesHoraCierre","viernesHoraCierre","viernesHoraApertura",
	"sabadoHoraApertura","sabadoHoraCierre","descripcion","local1","sucursalServicios"})
public class Sucursal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SUCURSAL_IDSUCURSAL_GENERATOR", sequenceName="SUCURSAL_SEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SUCURSAL_IDSUCURSAL_GENERATOR")
	@Column(name="id_sucursal")
	private Integer idSucursal;

	private String descripcion;

	@Column(name="domingo_hora_apertura")
	private Time domingoHoraApertura;

	@Column(name="domingo_hora_cierre")
	private Time domingoHoraCierre;

	@Column(name="jueves_hora_apertura")
	private Time juevesHoraApertura;

	@Column(name="jueves_hora_cierre")
	private Time juevesHoraCierre;

	@Column(name="lunes_hora_apertura")
	private Time lunesHoraApertura;

	@Column(name="lunes_hora_cierre")
	private Time lunesHoraCierre;

	@Column(name="martes_hora_apertura")
	private Time martesHoraApertura;

	@Column(name="martes_hora_cierre")
	private Time martesHoraCierre;

	@Column(name="miercoles_hora_apertura")
	private Time miercolesHoraApertura;

	@Column(name="miercoles_hora_cierre")
	private Time miercolesHoraCierre;

	private String nombre;

	@Column(name="sabado_hora_apertura")
	private Time sabadoHoraApertura;

	@Column(name="sabado_hora_cierre")
	private Time sabadoHoraCierre;

	@Column(name="viernes_hora_apertura")
	private Time viernesHoraApertura;

	@Column(name="viernes_hora_cierre")
	private Time viernesHoraCierre;

	//bi-directional many-to-one association to HorarioExcepcion
	@OneToMany(mappedBy="sucursal")
	private List<HorarioExcepcion> horarioExcepcions;

	//bi-directional many-to-one association to Ciudad
	@ManyToOne
	@JoinColumn(name="id_ciudad")
	private Ciudad ciudad;

	//bi-directional one-to-one association to Local
	@OneToOne
	@JoinColumn(name="id_sucursal")
	private Local local1;

	//bi-directional many-to-one association to Local
	@ManyToOne
	@JoinColumn(name="id_local")
	private Local local2;

	//bi-directional many-to-one association to Mapa
	@ManyToOne
	@JoinColumn(name="id_mapa")
	private Mapa mapa;

	//bi-directional many-to-one association to SucursalServicio
	@OneToMany(mappedBy="sucursal")
	private List<SucursalServicio> sucursalServicios;

	public Sucursal() {
	}

	public Integer getIdSucursal() {
		return this.idSucursal;
	}

	public void setIdSucursal(Integer idSucursal) {
		this.idSucursal = idSucursal;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Time getDomingoHoraApertura() {
		return this.domingoHoraApertura;
	}

	public void setDomingoHoraApertura(Time domingoHoraApertura) {
		this.domingoHoraApertura = domingoHoraApertura;
	}

	public Time getDomingoHoraCierre() {
		return this.domingoHoraCierre;
	}

	public void setDomingoHoraCierre(Time domingoHoraCierre) {
		this.domingoHoraCierre = domingoHoraCierre;
	}

	public Time getJuevesHoraApertura() {
		return this.juevesHoraApertura;
	}

	public void setJuevesHoraApertura(Time juevesHoraApertura) {
		this.juevesHoraApertura = juevesHoraApertura;
	}

	public Time getJuevesHoraCierre() {
		return this.juevesHoraCierre;
	}

	public void setJuevesHoraCierre(Time juevesHoraCierre) {
		this.juevesHoraCierre = juevesHoraCierre;
	}

	public Time getLunesHoraApertura() {
		return this.lunesHoraApertura;
	}

	public void setLunesHoraApertura(Time lunesHoraApertura) {
		this.lunesHoraApertura = lunesHoraApertura;
	}

	public Time getLunesHoraCierre() {
		return this.lunesHoraCierre;
	}

	public void setLunesHoraCierre(Time lunesHoraCierre) {
		this.lunesHoraCierre = lunesHoraCierre;
	}

	public Time getMartesHoraApertura() {
		return this.martesHoraApertura;
	}

	public void setMartesHoraApertura(Time martesHoraApertura) {
		this.martesHoraApertura = martesHoraApertura;
	}

	public Time getMartesHoraCierre() {
		return this.martesHoraCierre;
	}

	public void setMartesHoraCierre(Time martesHoraCierre) {
		this.martesHoraCierre = martesHoraCierre;
	}

	public Time getMiercolesHoraApertura() {
		return this.miercolesHoraApertura;
	}

	public void setMiercolesHoraApertura(Time miercolesHoraApertura) {
		this.miercolesHoraApertura = miercolesHoraApertura;
	}

	public Time getMiercolesHoraCierre() {
		return this.miercolesHoraCierre;
	}

	public void setMiercolesHoraCierre(Time miercolesHoraCierre) {
		this.miercolesHoraCierre = miercolesHoraCierre;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Time getSabadoHoraApertura() {
		return this.sabadoHoraApertura;
	}

	public void setSabadoHoraApertura(Time sabadoHoraApertura) {
		this.sabadoHoraApertura = sabadoHoraApertura;
	}

	public Time getSabadoHoraCierre() {
		return this.sabadoHoraCierre;
	}

	public void setSabadoHoraCierre(Time sabadoHoraCierre) {
		this.sabadoHoraCierre = sabadoHoraCierre;
	}

	public Time getViernesHoraApertura() {
		return this.viernesHoraApertura;
	}

	public void setViernesHoraApertura(Time viernesHoraApertura) {
		this.viernesHoraApertura = viernesHoraApertura;
	}

	public Time getViernesHoraCierre() {
		return this.viernesHoraCierre;
	}

	public void setViernesHoraCierre(Time viernesHoraCierre) {
		this.viernesHoraCierre = viernesHoraCierre;
	}

	public List<HorarioExcepcion> getHorarioExcepcions() {
		return this.horarioExcepcions;
	}

	public void setHorarioExcepcions(List<HorarioExcepcion> horarioExcepcions) {
		this.horarioExcepcions = horarioExcepcions;
	}

	public HorarioExcepcion addHorarioExcepcion(HorarioExcepcion horarioExcepcion) {
		getHorarioExcepcions().add(horarioExcepcion);
		horarioExcepcion.setSucursal(this);

		return horarioExcepcion;
	}

	public HorarioExcepcion removeHorarioExcepcion(HorarioExcepcion horarioExcepcion) {
		getHorarioExcepcions().remove(horarioExcepcion);
		horarioExcepcion.setSucursal(null);

		return horarioExcepcion;
	}

	public Ciudad getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public Local getLocal1() {
		return this.local1;
	}

	public void setLocal1(Local local1) {
		this.local1 = local1;
	}

	public Local getLocal2() {
		return this.local2;
	}

	public void setLocal2(Local local2) {
		this.local2 = local2;
	}

	public Mapa getMapa() {
		return this.mapa;
	}

	public void setMapa(Mapa mapa) {
		this.mapa = mapa;
	}

	public List<SucursalServicio> getSucursalServicios() {
		return this.sucursalServicios;
	}

	public void setSucursalServicios(List<SucursalServicio> sucursalServicios) {
		this.sucursalServicios = sucursalServicios;
	}

	public SucursalServicio addSucursalServicio(SucursalServicio sucursalServicio) {
		getSucursalServicios().add(sucursalServicio);
		sucursalServicio.setSucursal(this);

		return sucursalServicio;
	}

	public SucursalServicio removeSucursalServicio(SucursalServicio sucursalServicio) {
		getSucursalServicios().remove(sucursalServicio);
		sucursalServicio.setSucursal(null);

		return sucursalServicio;
	}

}