package py.com.prueba.modelo;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;


/**
 * The persistent class for the categoria database table.
 * 
 */
@Entity
@NamedQuery(name="Categoria.findAll", query="SELECT c FROM Categoria c")
@JsonIgnoreProperties({"locals"})
public class Categoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CATEGORIA_IDCATEGORIA_GENERATOR", sequenceName="CATEGORIA_SEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CATEGORIA_IDCATEGORIA_GENERATOR")
	@Column(name="id_categoria")
	private Integer idCategoria;

	private String nombre;

	//bi-directional many-to-many association to Local
	@ManyToMany
	@JoinTable(
		name="categoria_local"
		, joinColumns={
			@JoinColumn(name="id_categoria")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_local")
			}
		)
	private List<Local> locals;

	public Categoria() {
	}

	public Integer getIdCategoria() {
		return this.idCategoria;
	}

	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Local> getLocals() {
		return this.locals;
	}

	public void setLocals(List<Local> locals) {
		this.locals = locals;
	}

}