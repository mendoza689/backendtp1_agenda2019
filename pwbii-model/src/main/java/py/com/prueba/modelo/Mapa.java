package py.com.prueba.modelo;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the mapa database table.
 * 
 */
@Entity
@NamedQuery(name="Mapa.findAll", query="SELECT m FROM Mapa m")
@JsonIgnoreProperties({"latitud","longitud","sucursals","nombre"})
public class Mapa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MAPA_IDMAPA_GENERATOR", sequenceName="MAPA_SEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MAPA_IDMAPA_GENERATOR")
	@Column(name="id_mapa")
	private Integer idMapa;

	private String direccion;

	private BigDecimal latitud;

	private BigDecimal longitud;

	private String nombre;

	//bi-directional many-to-one association to Sucursal
	@OneToMany(mappedBy="mapa")
	private List<Sucursal> sucursals;

	public Mapa() {
	}

	public Integer getIdMapa() {
		return this.idMapa;
	}

	public void setIdMapa(Integer idMapa) {
		this.idMapa = idMapa;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public BigDecimal getLatitud() {
		return this.latitud;
	}

	public void setLatitud(BigDecimal latitud) {
		this.latitud = latitud;
	}

	public BigDecimal getLongitud() {
		return this.longitud;
	}

	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Sucursal> getSucursals() {
		return this.sucursals;
	}

	public void setSucursals(List<Sucursal> sucursals) {
		this.sucursals = sucursals;
	}

	public Sucursal addSucursal(Sucursal sucursal) {
		getSucursals().add(sucursal);
		sucursal.setMapa(this);

		return sucursal;
	}

	public Sucursal removeSucursal(Sucursal sucursal) {
		getSucursals().remove(sucursal);
		sucursal.setMapa(null);

		return sucursal;
	}

}