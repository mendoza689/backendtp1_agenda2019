package py.com.prueba.modelo;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;


/**
 * The persistent class for the ciudad database table.
 * 
 */
@Entity
@NamedQuery(name="Ciudad.findAll", query="SELECT c FROM Ciudad c")
@JsonIgnoreProperties({"sucursals"})
public class Ciudad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CIUDAD_IDCIUDAD_GENERATOR", sequenceName="CIUDAD_SEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CIUDAD_IDCIUDAD_GENERATOR")
	@Column(name="id_ciudad")
	private Integer idCiudad;

	private String nombre;

	//bi-directional many-to-one association to Sucursal
	@OneToMany(mappedBy="ciudad")
	private List<Sucursal> sucursals;

	public Ciudad() {
	}

	public Integer getIdCiudad() {
		return this.idCiudad;
	}

	public void setIdCiudad(Integer idCiudad) {
		this.idCiudad = idCiudad;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Sucursal> getSucursals() {
		return this.sucursals;
	}

	public void setSucursals(List<Sucursal> sucursals) {
		this.sucursals = sucursals;
	}

	public Sucursal addSucursal(Sucursal sucursal) {
		getSucursals().add(sucursal);
		sucursal.setCiudad(this);

		return sucursal;
	}

	public Sucursal removeSucursal(Sucursal sucursal) {
		getSucursals().remove(sucursal);
		sucursal.setCiudad(null);

		return sucursal;
	}

}