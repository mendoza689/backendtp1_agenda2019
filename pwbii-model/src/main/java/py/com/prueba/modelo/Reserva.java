package py.com.prueba.modelo;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the reserva database table.
 * 
 */
@Entity
@NamedQuery(name="Reserva.findAll", query="SELECT r FROM Reserva r")
public class Reserva implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RESERVA_IDRESERVA_GENERATOR", sequenceName="RESERVA_SEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RESERVA_IDRESERVA_GENERATOR")
	@Column(name="id_reserva")
	private Integer idReserva;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Column(name="fecha_hora_creacion")
	private Timestamp fechaHoraCreacion;

	@Column(name="flag_asistio")
	private String flagAsistio;

	@Column(name="flag_estado")
	private String flagEstado;

	@Column(name="hora_fin")
	private Time horaFin;

	@Column(name="hora_inicio")
	private Time horaInicio;

	private String observacion;

	//bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="id_cliente")
	private Persona persona1;

	//bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="id_empleado")
	private Persona persona2;

	//bi-directional many-to-one association to SucursalServicio
	@ManyToOne
	@JoinColumn(name="id_sucursal_servicio")
	private SucursalServicio sucursalServicio;

	public Reserva() {
	}

	public Integer getIdReserva() {
		return this.idReserva;
	}

	public void setIdReserva(Integer idReserva) {
		this.idReserva = idReserva;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Timestamp getFechaHoraCreacion() {
		return this.fechaHoraCreacion;
	}

	public void setFechaHoraCreacion(Timestamp fechaHoraCreacion) {
		this.fechaHoraCreacion = fechaHoraCreacion;
	}

	public String getFlagAsistio() {
		return this.flagAsistio;
	}

	public void setFlagAsistio(String flagAsistio) {
		this.flagAsistio = flagAsistio;
	}

	public String getFlagEstado() {
		return this.flagEstado;
	}

	public void setFlagEstado(String flagEstado) {
		this.flagEstado = flagEstado;
	}

	public Time getHoraFin() {
		return this.horaFin;
	}

	public void setHoraFin(Time horaFin) {
		this.horaFin = horaFin;
	}

	public Time getHoraInicio() {
		return this.horaInicio;
	}

	public void setHoraInicio(Time horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Persona getPersona1() {
		return this.persona1;
	}

	public void setPersona1(Persona persona1) {
		this.persona1 = persona1;
	}

	public Persona getPersona2() {
		return this.persona2;
	}

	public void setPersona2(Persona persona2) {
		this.persona2 = persona2;
	}

	public SucursalServicio getSucursalServicio() {
		return this.sucursalServicio;
	}

	public void setSucursalServicio(SucursalServicio sucursalServicio) {
		this.sucursalServicio = sucursalServicio;
	}

}