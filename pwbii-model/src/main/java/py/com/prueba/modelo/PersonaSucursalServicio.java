package py.com.prueba.modelo;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;


/**
 * The persistent class for the persona_sucursal_servicio database table.
 * 
 */
@Entity
@Table(name="persona_sucursal_servicio")
@NamedQuery(name="PersonaSucursalServicio.findAll", query="SELECT p FROM PersonaSucursalServicio p")
@JsonIgnoreProperties("precio")
public class PersonaSucursalServicio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONA_SUCURSAL_SERVICIO_IDPERSONASUCURSALSERVICIO_GENERATOR", sequenceName="PERSONA_SUCURSAL_SERVICIO_SEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONA_SUCURSAL_SERVICIO_IDPERSONASUCURSALSERVICIO_GENERATOR")
	@Column(name="id_persona_sucursal_servicio")
	private Integer idPersonaSucursalServicio;

	private BigDecimal precio;

	//bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="id_empleado")
	private Persona persona;

	//bi-directional many-to-one association to SucursalServicio
	@ManyToOne
	@JoinColumn(name="id_sucursal_servicio")
	private SucursalServicio sucursalServicio;

	public PersonaSucursalServicio() {
	}

	public Integer getIdPersonaSucursalServicio() {
		return this.idPersonaSucursalServicio;
	}

	public void setIdPersonaSucursalServicio(Integer idPersonaSucursalServicio) {
		this.idPersonaSucursalServicio = idPersonaSucursalServicio;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public SucursalServicio getSucursalServicio() {
		return this.sucursalServicio;
	}

	public void setSucursalServicio(SucursalServicio sucursalServicio) {
		this.sucursalServicio = sucursalServicio;
	}

}