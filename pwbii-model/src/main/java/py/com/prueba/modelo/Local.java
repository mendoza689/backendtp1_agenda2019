package py.com.prueba.modelo;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;


/**
 * The persistent class for the local database table.
 * 
 */
@Entity
@NamedQuery(name="Local.findAll", query="SELECT l FROM Local l")
@JsonIgnoreProperties({"categorias","personas","sucursal","sucursals","descripcion"})
public class Local implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="LOCAL_IDLOCAL_GENERATOR", sequenceName="LOCAL_SEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="LOCAL_IDLOCAL_GENERATOR")
	@Column(name="id_local")
	private Integer idLocal;

	private String descripcion;

	private String nombre;

	//bi-directional many-to-many association to Categoria
	@ManyToMany(mappedBy="locals")
	private List<Categoria> categorias;

	//bi-directional many-to-one association to Persona
	@OneToMany(mappedBy="local")
	private List<Persona> personas;

	//bi-directional one-to-one association to Sucursal
	@OneToOne(mappedBy="local1")
	private Sucursal sucursal;

	//bi-directional many-to-one association to Sucursal
	@OneToMany(mappedBy="local2")
	private List<Sucursal> sucursals;

	public Local() {
	}

	public Integer getIdLocal() {
		return this.idLocal;
	}

	public void setIdLocal(Integer idLocal) {
		this.idLocal = idLocal;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Categoria> getCategorias() {
		return this.categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public List<Persona> getPersonas() {
		return this.personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public Persona addPersona(Persona persona) {
		getPersonas().add(persona);
		persona.setLocal(this);

		return persona;
	}

	public Persona removePersona(Persona persona) {
		getPersonas().remove(persona);
		persona.setLocal(null);

		return persona;
	}

	public Sucursal getSucursal() {
		return this.sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public List<Sucursal> getSucursals() {
		return this.sucursals;
	}

	public void setSucursals(List<Sucursal> sucursals) {
		this.sucursals = sucursals;
	}

	public Sucursal addSucursal(Sucursal sucursal) {
		getSucursals().add(sucursal);
		sucursal.setLocal2(this);

		return sucursal;
	}

	public Sucursal removeSucursal(Sucursal sucursal) {
		getSucursals().remove(sucursal);
		sucursal.setLocal2(null);

		return sucursal;
	}

}