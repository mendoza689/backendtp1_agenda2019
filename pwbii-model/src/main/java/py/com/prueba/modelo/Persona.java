package py.com.prueba.modelo;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the persona database table.
 * 
 */
@Entity
@NamedQuery(name="Persona.findAll", query="SELECT p FROM Persona p")
@JsonIgnoreProperties({"sucursalServicio","local","personaSucursalServicios","reservas1","reservas2"})
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONA_IDPERSONA_GENERATOR", sequenceName="PERSONA_SEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONA_IDPERSONA_GENERATOR")
	@Column(name="id_persona")
	private Integer idPersona;

	private String apellido;

	private String email;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_nacimiento")
	private Date fechaNacimiento;

	@Column(name="flag_empleado")
	private String flagEmpleado;

	private String nombre;

	@Column(name="numero_documento")
	private String numeroDocumento;

	private String usuario;

	//bi-directional many-to-one association to Local
	@ManyToOne
	@JoinColumn(name="id_local")
	private Local local;

	//bi-directional many-to-one association to PersonaSucursalServicio
	@OneToMany(mappedBy="persona")
	private List<PersonaSucursalServicio> personaSucursalServicios;

	//bi-directional many-to-one association to Reserva
	@OneToMany(mappedBy="persona1")
	private List<Reserva> reservas1;

	//bi-directional many-to-one association to Reserva
	@OneToMany(mappedBy="persona2")
	private List<Reserva> reservas2;

	public Persona() {
	}

	public Integer getIdPersona() {
		return this.idPersona;
	}

	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getFlagEmpleado() {
		return this.flagEmpleado;
	}

	public void setFlagEmpleado(String flagEmpleado) {
		this.flagEmpleado = flagEmpleado;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumeroDocumento() {
		return this.numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Local getLocal() {
		return this.local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public List<PersonaSucursalServicio> getPersonaSucursalServicios() {
		return this.personaSucursalServicios;
	}

	public void setPersonaSucursalServicios(List<PersonaSucursalServicio> personaSucursalServicios) {
		this.personaSucursalServicios = personaSucursalServicios;
	}

	public PersonaSucursalServicio addPersonaSucursalServicio(PersonaSucursalServicio personaSucursalServicio) {
		getPersonaSucursalServicios().add(personaSucursalServicio);
		personaSucursalServicio.setPersona(this);

		return personaSucursalServicio;
	}

	public PersonaSucursalServicio removePersonaSucursalServicio(PersonaSucursalServicio personaSucursalServicio) {
		getPersonaSucursalServicios().remove(personaSucursalServicio);
		personaSucursalServicio.setPersona(null);

		return personaSucursalServicio;
	}

	public List<Reserva> getReservas1() {
		return this.reservas1;
	}

	public void setReservas1(List<Reserva> reservas1) {
		this.reservas1 = reservas1;
	}

	public Reserva addReservas1(Reserva reservas1) {
		getReservas1().add(reservas1);
		reservas1.setPersona1(this);

		return reservas1;
	}

	public Reserva removeReservas1(Reserva reservas1) {
		getReservas1().remove(reservas1);
		reservas1.setPersona1(null);

		return reservas1;
	}

	public List<Reserva> getReservas2() {
		return this.reservas2;
	}

	public void setReservas2(List<Reserva> reservas2) {
		this.reservas2 = reservas2;
	}

	public Reserva addReservas2(Reserva reservas2) {
		getReservas2().add(reservas2);
		reservas2.setPersona2(this);

		return reservas2;
	}

	public Reserva removeReservas2(Reserva reservas2) {
		getReservas2().remove(reservas2);
		reservas2.setPersona2(null);

		return reservas2;
	}

}