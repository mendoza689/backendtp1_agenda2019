



package py.com.prueba.rest;

import java.net.URI;
import java.net.URLEncoder;
import java.sql.Time;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import javax.ws.rs.core.UriInfo;

import py.com.prueba.ejb.ReservaEJB;
import py.com.prueba.ejb.SucursalEJB;
import py.com.prueba.modelo.Servicio;
import py.com.prueba.modelo.Sucursal;

@Path("reserva")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class ReservaRest {


    @Inject
    private ReservaEJB reservaEJB;
    @Context
    protected UriInfo uriInfo;


    @GET
    @Path("/disponible")
    public Response listar(@QueryParam("idSucursalServicio") int idSucursalServicio,@QueryParam("fecha") String fecha,@QueryParam("idEmpleado") int idEmpleado) throws WebApplicationException, ParseException{

    		
    	ArrayList<Map<String, String>> listEntity = null;
        Long total = null;
        total = reservaEJB.total();
        listEntity = reservaEJB.list_hor(fecha,idSucursalServicio,idEmpleado);

        Map<String,Object> mapaResultado=new HashMap<String, Object>();
        
        mapaResultado.put("horarios", listEntity);
   
        return Response.ok(mapaResultado).build();
        
        

    }
   
 
}
