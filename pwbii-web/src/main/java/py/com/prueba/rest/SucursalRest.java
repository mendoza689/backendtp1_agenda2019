

package py.com.prueba.rest;

import java.net.URI;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import py.com.prueba.ejb.PersonaEJB;
import py.com.prueba.ejb.SucursalEJB;
import py.com.prueba.modelo.Local;
import py.com.prueba.modelo.Persona;
import py.com.prueba.modelo.Sucursal;

@Path("sucursal")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class SucursalRest {


    @Inject
    private SucursalEJB sucursalEJB;
    @Context
    protected UriInfo uriInfo;


    @GET
    @Path("/")
    public Response listar(@QueryParam("idCategoria") int idCategoria) throws WebApplicationException{

        List<Sucursal> listEntity = null;
        Long total = null;
        total = sucursalEJB.total();
        listEntity = sucursalEJB.suc_cat(idCategoria);
        Map<String,Object> mapaResultado=new HashMap<String, Object>();
        mapaResultado.put("lista", listEntity);
        mapaResultado.put("total", listEntity.size());

        return Response.ok(mapaResultado).build();

    }
   
 
}
