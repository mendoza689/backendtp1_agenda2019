package py.com.prueba.rest;




import java.net.URI;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import javax.ws.rs.core.UriInfo;

import py.com.prueba.ejb.EspecialidadEJB;
import py.com.prueba.ejb.ProfesionalEJB;
import py.com.prueba.modelo.Especialidad;
import py.com.prueba.modelo.PersonaSucursalServicio;
import py.com.prueba.modelo.SucursalServicio;

@Path("profesional")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class ProfesionalRest {


    @Inject
    private ProfesionalEJB profesionalEJB;
    @Context
    protected UriInfo uriInfo;


    @GET
    @Path("/")
    public Response listar(@QueryParam("idSucursalServicio") int idSucursalServicio) throws WebApplicationException{

        List<PersonaSucursalServicio> listEntity = null;
        Long total = null;
        total = profesionalEJB.total();
        listEntity = profesionalEJB.per_suc(idSucursalServicio);
        Map<String,Object> mapaResultado=new HashMap<String, Object>();
        mapaResultado.put("lista", listEntity);
        mapaResultado.put("total", listEntity.size());
        return Response.ok(mapaResultado).build();

    }
   
 
}
