package py.com.prueba.rest.json;
import py.com.prueba.modelo.Ciudad;
import py.com.prueba.modelo.Local;
import py.com.prueba.modelo.Mapa;

public class SucbyCDL_Json {
	public Integer idSucursal;
	public String nombresucursal;
	public Ciudad ciudad;
	public Local local;
	public Mapa mapa;
	
	public SucbyCDL_Json( Integer idSucursal,String nombre,Ciudad ciudad,Local local, Mapa mapa) {
		this.idSucursal=idSucursal;
		this.nombresucursal=nombre;
		this.ciudad=ciudad;
		this.local=local;
		this.mapa=mapa;
		
	}

}
