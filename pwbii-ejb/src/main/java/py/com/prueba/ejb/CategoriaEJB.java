package py.com.prueba.ejb;



import py.com.prueba.modelo.Agenda;
import py.com.prueba.modelo.Categoria;
import py.com.prueba.modelo.Persona;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Stateless
public class CategoriaEJB {


    @PersistenceContext(unitName="pwbiiPU")
    private EntityManager em;

    @Inject
    private PersonaEJB personaEJB;

    protected EntityManager getEm() {
        return em;
    }

    public Categoria get(Integer id) {
        return em.find(Categoria.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Categoria> lista() {
        Query q = getEm().createQuery(
                "SELECT p FROM Categoria p");
        return (List<Categoria>) q.getResultList();
    }
    public Long total() {
        Query q = getEm().createQuery(
                "Select Count(p) from Categoria p");
        return (Long) q.getSingleResult();
    }
}
