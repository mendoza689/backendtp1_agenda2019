
package py.com.prueba.ejb;
import py.com.prueba.modelo.Especialidad;
import py.com.prueba.modelo.Sucursal;
import py.com.prueba.rest.json.SucbyCDL_Json;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.List;

@Stateless

public class EspecialidadEJB {

    @PersistenceContext(unitName="pwbiiPU")
    private EntityManager em;

    @Inject
    private SucursalEJB sucursalEJB;


    protected EntityManager getEm() {
        return em;
    }

    public Especialidad get(Integer id) {
        return em.find(Especialidad.class, id);
    
    }

   
    @SuppressWarnings("unchecked")
    public List<Especialidad> esp_serv(int idSucursal) {
    	 List<Especialidad> Listsuc=new ArrayList<Especialidad>();
    	
    	try {
    		Sucursal suc=sucursalEJB.get(idSucursal);
            Query q = getEm().createQuery( "Select e from Especialidad e,Servicio se,SucursalServicio sus "
    + "where se.especialidad=e and  se=sus.servicio and sus.sucursal=:suc ").setParameter("suc", suc);
            
           Listsuc=(List<Especialidad>) q.getResultList();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
    
      
        return Listsuc;        
        
        
    }

    public Long total() {
        Query q = getEm().createQuery(
                "Select Count(p) from Especialidad p");
        return (Long) q.getSingleResult();
    }
}
