


package py.com.prueba.ejb;




import py.com.prueba.modelo.Categoria;
import py.com.prueba.modelo.Local;
import py.com.prueba.modelo.Sucursal;
import py.com.prueba.rest.json.SucbyCDL_Json;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.List;




@Stateless
public class SucursalEJB {
    @PersistenceContext(unitName="pwbiiPU")
    private EntityManager em;

    @Inject
    private CategoriaEJB categoriaEJB;


	private List <SucbyCDL_Json> cdl;

    protected EntityManager getEm() {
        return em;
    }

    public Sucursal get(Integer id) {
        return em.find(Sucursal.class, id);
    }

   
    @SuppressWarnings("unchecked")
    public List<Sucursal> suc_cat(int idCategoria) {
    	 List<Sucursal> Listsuc= new ArrayList <Sucursal>();
    	try {
    		Categoria cat=categoriaEJB.get(idCategoria);
        	Query q = getEm().createQuery( "Select suc from Sucursal suc, Local loc where suc.local2=loc and :cat MEMBER OF loc.categorias").setParameter("cat", cat);
        	 Listsuc=(List<Sucursal>) q.getResultList();
		} catch (Exception e) {
			
		}
    	
        return Listsuc;        
        
        
    }

    public Long total() {
        Query q = getEm().createQuery(
                "Select Count(p) from Categoria p");
        return (Long) q.getSingleResult();
    }
}
