package py.com.prueba.ejb;




import py.com.prueba.ejb.PersonaEJB;

import py.com.prueba.modelo.Sucursal;
import py.com.prueba.modelo.SucursalServicio;


import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Stateless

public class SucursalServicioEJB  {
    @PersistenceContext(unitName="pwbiiPU")
    private EntityManager em;




    protected EntityManager getEm() {
        return em;
    }

    public SucursalServicio get(Integer id) {
        return em.find(SucursalServicio.class, id);
    }

    
  
}
