
package py.com.prueba.ejb;




import py.com.prueba.modelo.Persona;
import py.com.prueba.modelo.Reserva;
import py.com.prueba.modelo.Sucursal;
import py.com.prueba.modelo.SucursalServicio;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Stateless

public class ReservaEJB  {
    @PersistenceContext(unitName="pwbiiPU")
    private EntityManager em;

    @Inject
    private SucursalServicioEJB sucursalServicioEJB;
    @Inject
    private SucursalEJB sucursalEJB;
    @Inject
    private PersonaEJB personaEJB;


    protected EntityManager getEm() {
        return em;
    }

    public Reserva get(Integer id) {
        return em.find(Reserva.class, id);
    }

    
    private  Date stringtodate (String fecha) throws ParseException {
    	
    	 SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
         Date date = formatter.parse(fecha);
         
         return date;
    	
    }
    
    
    private  String  diasemana (Date fecha) {
    	   DateFormat format2=new SimpleDateFormat("EEEE"); 
           String diasem=format2.format(fecha);
           
           if(diasem.equals("sábado"))
           {
        	   diasem="sabado";
           }
        return diasem;
   	
   }

    @SuppressWarnings("unchecked")
    public ArrayList<Map<String, String>> list_hor(String fecha, int idSucursalServicio, int idEmpleado) throws ParseException {
    	
    	Persona persona=null;
    	try {
    		persona=personaEJB.get(idEmpleado);
		} catch (Exception e) {
			
		}
         Date date = this.stringtodate(fecha);
         String diasem=this.diasemana(date);
   

    	SucursalServicio serv=sucursalServicioEJB.get(idSucursalServicio);
    	Sucursal obsuc=serv.getSucursal();
    
        Time horaapertura=(Time) getEm().createQuery("Select s."+diasem+"HoraApertura from Sucursal s WHERE s=:suc").setParameter("suc", obsuc).getSingleResult();
        Time horacierre=(Time) getEm().createQuery("Select s."+diasem+"HoraCierre from Sucursal s WHERE s=:suc").setParameter("suc", obsuc).getSingleResult();
       
        int duracion=serv.getDuracion();
    	int capacidadN=serv.getCapacidad();
        LocalTime lthoraapertura = horaapertura.toLocalTime();
        LocalTime lthoracierre = horacierre.toLocalTime();
        LocalTime iterlocaltime=lthoraapertura;
        
        ArrayList<Map<String,String>> almap=new ArrayList<Map<String,String>>();
        try {
	        while ((iterlocaltime.plusMinutes(duracion)).compareTo(lthoracierre)<=0) {
	        	
	        	
	        	//si existe una exepción del horario
	        	Long qcrs =(Long) getEm().createQuery("SELECT Count(r) FROM Reserva r WHERE r.sucursalServicio=:sucserv and r.fecha=:fecha and r.horaInicio=:hapertura and r.horaFin=:hcierre and r.flagEstado='R'")
	        						.setParameter("sucserv", serv)
	        						.setParameter("fecha", date,TemporalType.DATE)
	        						.setParameter("hapertura", Time.valueOf(iterlocaltime))
	        						.setParameter("hcierre", Time.valueOf(iterlocaltime.plusMinutes(duracion))).getSingleResult();
	        
	        	//numero total de reservas con flag R 
	        	Long qhe=(Long) getEm().createQuery("SELECT Count(h) FROM HorarioExcepcion h WHERE h.sucursal=:suc and h.fecha=:fecha and h.horaApertura=:hapertura and h.horaCierre=:hcierre")
						.setParameter("suc", obsuc)
						.setParameter("fecha", date,TemporalType.DATE)
						.setParameter("hapertura", Time.valueOf(iterlocaltime))
						.setParameter("hcierre", Time.valueOf(iterlocaltime.plusMinutes(duracion))).getSingleResult();
	        	
	        	Long esr=(long) 0;
	        	
	        	//empleado tiene ya la reserva
	        	if(persona!=null) {
	        		
	        		esr= (Long) getEm().createQuery("SELECT Count(r) FROM Reserva r WHERE r.sucursalServicio=:sucserv and r.fecha=:fecha and r.horaInicio=:hapertura and r.horaFin=:hcierre and r.flagEstado='R' and r.persona2=:empleado")
    						.setParameter("sucserv", serv)
    						.setParameter("fecha", date,TemporalType.DATE)
    						.setParameter("hapertura", Time.valueOf(iterlocaltime))
    						.setParameter("hcierre", Time.valueOf(iterlocaltime.plusMinutes(duracion)))
    						.setParameter("empleado", persona)
    						.getSingleResult();
	        	}
	        	

	        	if(qhe==0 && qcrs<capacidadN && esr==0 ) {
	        		
	        		 Map<String,String> temp=new HashMap<String,String>();
	            	 temp.put("fecha",fecha);
	                 temp.put("horaInicio", iterlocaltime.toString());
	                 iterlocaltime=iterlocaltime.plusMinutes(duracion);
	                 temp.put("horaSalida", iterlocaltime.toString());
	                 almap.add(temp);
	        		
	        	}else {
	        		
	        		 iterlocaltime=iterlocaltime.plusMinutes(duracion);
	        	}
	        	
	        }
        }catch(Exception e) {
        	
        	almap=new ArrayList<Map<String,String>>();
        	
        }
        return almap;        
    }
    

    public Long total() {
        Query q = getEm().createQuery(
                "Select Count(p) from Sucursal p");
        return (Long) q.getSingleResult();
    }
}
