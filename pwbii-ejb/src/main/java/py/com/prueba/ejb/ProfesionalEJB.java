package py.com.prueba.ejb;

import py.com.prueba.modelo.PersonaSucursalServicio;
import py.com.prueba.modelo.Sucursal;
import py.com.prueba.modelo.SucursalServicio;
import py.com.prueba.rest.json.SucbyCDL_Json;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.List;


@Stateless
public class ProfesionalEJB {
    @PersistenceContext(unitName="pwbiiPU")
    private EntityManager em;

    @Inject
    private PersonaEJB personaEJB;


	private List <SucbyCDL_Json> cdl;

    protected EntityManager getEm() {
        return em;
    }

    public Sucursal get(Integer id) {
        return em.find(Sucursal.class, id);
    }


    @SuppressWarnings("unchecked")
    public List<PersonaSucursalServicio> per_suc(int idSucursalServicio) {
    	List<PersonaSucursalServicio>  lpsus=new ArrayList<PersonaSucursalServicio>();
    	try {
    		SucursalServicio sucs=(SucursalServicio)getEm().createQuery("select s from SucursalServicio s where s.idSucursalServicio=:idSucursalServicio").setParameter("idSucursalServicio", idSucursalServicio).getSingleResult();
    		   
    	    lpsus=(List<PersonaSucursalServicio>)getEm().createQuery("select s from PersonaSucursalServicio s where s.sucursalServicio=:SucursalServicio").setParameter("SucursalServicio", sucs).getResultList();
			
		} catch (Exception e) {
			// TODO: handle exception
		}

    	return lpsus;
    
        
        
    }

    public Long total() {
        Query q = getEm().createQuery(
                "Select Count(p) from SucursalServicio p");
        return (Long) q.getSingleResult();
    }
}
